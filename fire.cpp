/*
 * Author: arr
 * License: MIT : https://opensource.org/licenses/MIT
 */

#include "fire.h"

PROGMEM const uint8_t fire[] =
{
  0x0, 0x0,
  0x1, 0x0,
  0x2, 0x0,
  0x3, 0x0,
  0x0, 0x1,
  0x1, 0x1,
  0x2, 0x1,
  0x3, 0x1,
  0x0, 0x2,
  0x1, 0x2,
  0x2, 0x2,
  0x3, 0x2,
  0x0, 0x3,
  0x1, 0x3,
  0x2, 0x3,
  0x3, 0x3,
};

const int sprite_count = 16;
const int fire_h = 2;
const int fire_w = 2;

const uint8_t *compute_sprite_offset(int idx)
{
  return (fire + (((sizeof(fire) / sizeof(fire[0])) / sprite_count) * idx));
}

int this_sprite_index(int py, uint8_t c1, uint8_t c2) {
  switch (py % 8)
  {
    case 0:
      return ((c2 & 0x3) << 2) | (c1 & 0x3);
    case 2:
      return (c2 & 0xc) | ((c1 & 0xc) >> 2);
    case 4:
      return ((c2 & 0x30) >> 2) | ((c1 & 0x30) >> 4);
    case 6:
      return ((c2 & 0xc0) >> 4) | ((c1 & 0xc0) >> 6);
    default:
      return 0;
  }
}

fire_t::fire_t(uint8_t o_px, uint8_t o_py, uint8_t e_px, uint8_t e_py)
  : _o_px(o_px)
  , _o_py(o_py)
  , _e_px(e_px)
  , _e_py(e_py)
{ }

void fire_t::render(Arduboy2 &a) {
  // Represents the current row for the fire loop - set to maximum allowable size
  uint8_t fire_row[WIDTH / fire_w] = { 0 };

  const int fire_row_w = _e_px - _o_px;
  const uint8_t *fb = a.getBuffer();
  for (int y = _o_py; y < _e_py; y += fire_h) {
    for (int x = _o_px; x < _e_px; x += fire_w) {
      int new_idx_maybe;
      if ((y + fire_h) >= _e_py) {
        new_idx_maybe = random(2) ? 0 : (sprite_count - 1);
      } else {
        new_idx_maybe = compute_new_sprite(x, y, fb) % sprite_count;
      }

      fire_row[x / fire_w] = new_idx_maybe;
    }

    // Clear current fire row
    a.fillRect(_o_px, y, fire_row_w, fire_h, BLACK);

    // Update current fire row
    for (int x = _o_px; x < _e_px; x += fire_w) {
      const uint8_t *sp = compute_sprite_offset(fire_row[x / fire_w]);
      a.drawBitmap(x, y, sp, fire_w, fire_h, WHITE);
    }
  }
}

// Compute new fire sprite index
// Average of w, x, y, z, and c yields the new sprite index for c.
//
//  . . c c . .
//  . . c c . .
//  x x y y z z
//  x x y y z z
//  . . w w . .
//  . . w w . .
//
int fire_t::compute_new_sprite(int px, int py, const uint8_t *fb) {
  // Handle the vertical column first (c, y, w)
  const int fb_c_idx = ((py / 8) * WIDTH) + px;
  int accum = this_sprite_index(py, fb[fb_c_idx], fb[fb_c_idx + 1]);

  const int y_py = py + fire_h;
  if (y_py < _e_py) {
    const int fb_y_idx = ((y_py / 8) * WIDTH) + px;
    accum += this_sprite_index(y_py, fb[fb_y_idx], fb[fb_y_idx + 1]);
  }

  const int w_py = py + fire_h + fire_h;
  if (w_py < (_e_py - (fire_h + fire_h))) {
    const int fb_w_idx = ((w_py / 8) * WIDTH) + px;
    accum += this_sprite_index(w_py, fb[fb_w_idx], fb[fb_w_idx + 1]);
  }

  // 'x'
  if (px > 0) {
    const int x_py = y_py;
    const int fb_x_idx = ((x_py / 8) * WIDTH) + (px - fire_w);
    accum += this_sprite_index(x_py, fb[fb_x_idx], fb[fb_x_idx + 1]);
  }

  // 'z'
  if ((px + fire_w) < _e_px) {
    const int z_py = y_py;
    const int fb_z_idx = ((z_py / 8) * WIDTH) + (px + fire_w);
    accum += this_sprite_index(z_py, fb[fb_z_idx], fb[fb_z_idx + 1]);
  }

  return (accum / 5);
}
