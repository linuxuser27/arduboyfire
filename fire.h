/*
 * Author: arr
 * License: MIT : https://opensource.org/licenses/MIT
 */

#include "Arduboy2.h"

class fire_t
{
public:
  // Create new fire_t instance within the supplied bounding box.
  //   o . . .
  //   .     .
  //   . . . e
  // The computed width and height should be divisible by 2 (i.e. even),
  // but can represent any quadrilateral defined by 2 points.
  fire_t(uint8_t o_px = 0, uint8_t o_py = 0, uint8_t e_px = WIDTH, uint8_t e_py = HEIGHT);

  // Render the next fire frame
  void render(Arduboy2 &a);

private:
  int compute_new_sprite(int px, int py, const uint8_t *fb);

private:
  const uint8_t _o_px, _o_py, _e_px, _e_py;
};

