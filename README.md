# ArduboyFire #

A demo of fire animation on an [Arduboy](https://www.arduboy.com/).

### Dependencies ###

[Arduboy2](https://github.com/MLXXXp/Arduboy2)

### License ###

The source is available under the MIT license.