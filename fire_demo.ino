/*
 * Author: arr
 * License: MIT : https://opensource.org/licenses/MIT
 */

#include "Arduboy2.h"
#include "fire.h"

Arduboy2 arduboy;

void setup() {
  arduboy.boot();
  arduboy.setFrameRate(60);

  arduboy.initRandomSeed(); // Needed for fire generation
  arduboy.clear();
}

fire_t ul(0,0,32,32);
fire_t ur(0,32,32,64);
fire_t ll(96, 0, 128, 32);
fire_t lr(96, 32, 128, 64);
fire_t c(40, 16, 88, 48);

void loop() {
  if (!(arduboy.nextFrame()))
    return;

  ul.render(arduboy);
  ur.render(arduboy);
  ll.render(arduboy);
  lr.render(arduboy);
  c.render(arduboy);

  arduboy.display();
}
